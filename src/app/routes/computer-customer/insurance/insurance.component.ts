import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { OrderService } from 'src/app/services/computer-management/order/order.service';

@Component({
  selector: 'app-insurance',
  templateUrl: './insurance.component.html',
  styleUrls: ['./insurance.component.less'],
})
export class InsuranceComponent implements OnInit {
  serialFilter = '';
  noResult = false;
  result = false;
  insuranceModel: any;
  datas: any[] = [];
  constructor(private orderService: OrderService, private nzMessage: NzMessageService) {}
  ngOnInit(): void {}
  checkInsuranceBySerial() {
    if (this.serialFilter === '' || this.serialFilter == undefined || this.serialFilter === null) {
      this.nzMessage.error('Bạn hãy nhập serial trước');
      return;
    }
    this.orderService
      .checkInsurance(this.serialFilter)
      .toPromise()
      .then((res) => {
        if (res.data) {
          this.insuranceModel = res.data;
          this.datas.push(res.data);
          this.result = true;
          this.noResult = false;
        } else {
          this.result = false;
          this.noResult = true;
        }
      })
      .catch((err) => {
        this.result = false;
        this.noResult = true;
      });
  }
}
